# Makefile per curriculum vitae in LaTeX

default: all

all:
	#make clean
	xelatex cv-rinnone-ita.tex
	#makeindex -s index.ist cv-ita.idx
	bibtex cv-rinnone-ita	
	xelatex cv-rinnone-ita.tex
	xelatex cv-rinnone-ita.tex
	gs -sDEVICE=pdfwrite -dPDFSETTINGS=/default -dNOPAUSE -dQUIET -dBATCH -sOutputFile=cv-rinnone-ita.pdf.tmp cv-rinnone-ita.pdf
	mv cv-rinnone-ita.pdf.tmp cv-rinnone-ita.pdf	
	
.tex:
	xelatex $@
	#dvips -t a4 $?.dvi -o
	#ps2pdf $@.ps
	
clean:
	rm -f *.aux
	rm -f *.log
	rm -f *.toc
	rm -f *.loa
	rm -f *.lot
	rm -f *.lof
	rm -f *.idx
	rm -f *.bbl
	rm -f *.blg
	rm -f *.idx
	rm -f *.ilg
	rm -f *.ind
	rm -f *.ps
	rm -f *.out
	rm -f *.nav
	rm -f *.snm
	rm -f *.bcf
	rm -f *.xml
	rm -f *~
erase:
	make clean
	rm -f *.dvi*
	rm -f *.ps*
	rm -f *.pdf*

help:
	@echo "make [all]: genera i file .dvi e .pdf di tutti i file .tex"
	@echo "make theis: genera i file .dvi e .pdf " 
	@echo "make clean: elimina i file non necessari"
	@echo "make erase: elimina i file non necessari e i file .dvi, .ps e .pdf"
	@echo "Leggere anche README"
